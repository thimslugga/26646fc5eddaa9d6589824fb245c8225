#!/bin/sh

python /root/ipmi-updater.py \
  --ipmi-url $IPMI_URL \
  --model $MODEL \
  --username $USER \
  --password $PASS \
  --key-file $KEY_FILE \
  --cert-file $CERT_FILE \
  ${NO_REBOOT:+ --no-reboot}${FORCE_UPDATE:+ --force-update}${QUIET:+ --quiet}${DEBUG:+ --debug}
